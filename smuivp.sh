#
# Copyright (c) 1994 Hartmut Becker
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# Except as contained in this notice, the name(s) of the above
# copyright holders shall not be used in advertising or otherwise
# to promote the sale, use or other dealings in this Software
# without prior written authorization.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# ABSTRACT:
#
#	IVP for smu, simple message utility
#	this is also a simple example
#
# ENVIRONMENT:
#
#	smu, shell
#
# AUTHOR:	Hartmut Becker			CREATION DATE:	18-JAN-1994
#
# MODIFIED BY

#	create a ivp message file
cat <<EOF >ivp.msg
!++
!
! ABSTRACT:
!
!	IVP message file for smu, simple message utility
!
!--

.TITLE SMUIVP, installation verification procedure for smu
.IDENT 'X-1'
.FACILITY IVP, 001 /PREFIX=IVP_

.BASE 0
.SEVERITY SUCCESS
!
! Success messages for ivp
!
START		<IVP started.>
END		<IVP normal end.>
.BASE 4
.SEVERITY INFORMATIONAL
!
! Informational messages for ivp
!
IGNORE		<ignore all following messages upto (incl.) IVP-F-FATEND>
NOINF		<this informal is a faked informal message.>
SWITCHWAR	<switch to warning messages.>
IGNNOMSG	<ingore the following NOMSG message>
.BASE 8
.SEVERITY WARNING
!
! Warning messages for ivp.
!
NOWARN		<this warning is a faked warning message.>
SWITCHERR	<switch to error messages.>

.BASE 12
.SEVERITY ERROR
!
! Error messages for ivp.
!
NOERR		<this error is a faked error message.>
SWITCHFAT	<switch to fatal messages.>

.BASE 16
.SEVERITY FATAL
!
! Fatal messages for ivp.
!
NOFAT		<this fatal error is a faked fatal error message.>
FATEND		<last fatal test message.>

.END
EOF

#
#	use smu to generate source and include file
./smu ivp.msg

#	create a test source
cat <<EOF >smuivp.c
/*	ANSIs */

#include	<stdio.h>
#include	<stdlib.h>

/*	APPLs */
#include	"ivp.h"

/*	global static */
static unsigned int	codes[]= {
	IVP_IGNORE,
	IVP_NOINF,
	IVP_SWITCHWAR,
	IVP_NOWARN,
	IVP_SWITCHERR,
	IVP_NOERR,
	IVP_SWITCHFAT,
	IVP_NOFAT,
	IVP_FATEND,
};

int	main (void) {

	int i;
	printf ("%s\n", getmsg(IVP_START));

	for (i=0; i<sizeof codes/sizeof(unsigned int); i++)
		printf ("%s\n", getmsg(codes[i]));

	printf ("%s\n", getmsg(IVP_IGNNOMSG));
	printf ("%s\n", getmsg(0x8b8b));
	printf ("%s\n", getmsg(IVP_END));

	return EXIT_SUCCESS;
}
EOF

#	make the ivp image
cc -o smuivp smuivp.c ivp.c

#	run it
./smuivp

#	cleanup
rm -f smuivp smuivp.c smuivp.o ivp.c ivp.h ivp.o ivp.msg
